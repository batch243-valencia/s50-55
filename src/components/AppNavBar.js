// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from "react-router-dom";
import { Nav, Navbar } from 'react-bootstrap';
import '../App.css';
import { useContext,useEffect} from "react";

import UserContext from "../UserContext";
export default function AppNavBar() {
  const {user}=useContext(UserContext)
  // console.log(user)
// useEffect(() => {
//     setUser(localStorage.getItem('email'));
//   },[user]);
//   console.log(user)
  return (
    <Navbar bg="light" expand="lg" className="vw-100">
      <Navbar.Brand as={NavLink} to="/">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
          {!user&&<Nav.Link as={NavLink} to="/register">Register</Nav.Link>}
          {!user&&<Nav.Link as={NavLink} to="/login">Login</Nav.Link>}
          {user&&<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}