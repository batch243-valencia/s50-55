import {Row,Col, Card} from "react-bootstrap/";

export default function HighLights(){
    return (
        
            <Row  className="w-100">
                <Col sm={12}  lg={4}>
                {/* FIRST */}
                <Card className="h-100">
                        <Card.Body>
                            <Card.Title>
                                <h2>Learn From Home</h2>
                            </Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id perspiciatis nemo neque recusandae, iure tenetur modi error. Pariatur, officiis facilis?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                {/* Second */}
                <Col sm={12}  lg={4}>
                    <Card className="h-100">
                        <Card.Body>
                            <Card.Title>Study Now, Pay Later</Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae voluptate, placeat corporis amet veniam eveniet labore velit distinctio. Debitis, sed?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                {/* Third */}
                <Col sm={12}  lg={4}>
                    <Card className="h-100">
                        <Card.Body>
                            <Card.Title>Be Part of our community</Card.Title>
                            <Card.Text>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Enim deleniti minus earum voluptate provident, recusandae placeat corporis sit, neque quas ratione reprehenderit harum unde cupiditate soluta non nulla fugiat laudantium?
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        
    )
}