import {Button,Row,Col} from "react-bootstrap/";
import {Link} from 'react-router-dom'; 

export default function Banner(){
    return (

        <Row className="text-center">
            <Col className="col-12">
                <h1>Zuitt Coding BootCamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button as = {Link} to = "/Courses"> Enroll now! </Button>
            </Col>
        </Row>

    )
}