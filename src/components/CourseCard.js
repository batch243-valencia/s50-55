import {useEffect, useState,useContext} from 'react';
import {Button,Card, Row, Col}from 'react-bootstrap/'
import UserContext from "../UserContext";
import {Link} from 'react-router-dom';
// const SampleCourse = {
//     "courseName":"SampleCourse",
//     "description":"This is a sample course offering",
//     "price":40000
// }

// function enroll
export default function CourseCard({Courseprop}){
    const{name,description,price,slots}=Courseprop
    const [enrollees,setEnrollees] = useState(0);
    const [slotsAvailable,setSlotsAvailable] = useState(slots);
    const {user} =useContext(UserContext);
    const [notAvailable,setNotAvailable] = useState(slotsAvailable===0)
    useEffect(()=>{
        if (slotsAvailable===0) {
            setNotAvailable(true)
        }
    },[slotsAvailable]);


    
    function enroll(){
        if (notAvailable){alert("SORRY, No More Slots")}
        if (slotsAvailable>0){
            setEnrollees(enrollees+1)
            setSlotsAvailable(slotsAvailable-1)        
        }
    }
    return (

            <Row >
            <Col className=''> 
                <Card className='courseCard'>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{NumberToCurrency(price)}</Card.Text>
                    <Card.Subtitle>Slots:</Card.Subtitle>
                        <Card.Text>{slotsAvailable} slots</Card.Text>
                    <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{enrollees}</Card.Text>
                    {
                        (user)
                        ?<Button  variant="primary" onClick={enroll} disabled={notAvailable}>Enroll</Button>
                        :<Button as = {Link} to ="/login"
                         variant="primary" disabled={notAvailable}>Login</Button>
                    }
                    


                </Card.Body>
                </Card>
                </Col>    
            </Row>

    )
}
function NumberToCurrency(price){
return price.toLocaleString('fil-PH', {maximumFractionDigits:2, style:'currency', currency:'PHP', useGrouping:true})
}