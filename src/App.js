import './App.css';
import Courses from './pages/Courses';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import {BrowserRouter as Router, Routes,Route} from 'react-router-dom'
// import { Container} from "react-bootstrap";
import Error404 from './pages/Error404';
import {useEffect, useState} from 'react';
import { UserProvider } from './UserContext';
function App() {
  const [email,setEmail] = useState('');
  const [user,setUser] = useState(localStorage.getItem("email"));
  const unSetUser = () =>{
   localStorage.setItem('email',email)
        setUser(localStorage.getItem("email"))
        setEmail('');
        
  }
  return (
    <>
    <UserProvider value={{ user, setUser, unSetUser }}>
    <Router>
      <AppNavBar />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/courses" element={<Courses />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/Logout" element={<Logout />} />
        <Route path="/*" element={<Error404 />} />
      </Routes>

    </Router>
    </UserProvider></>
  );
}
export default App;