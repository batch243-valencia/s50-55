import { Button, Col, Container, Form, Row } from "react-bootstrap";
import {useEffect, useState,useContext} from 'react'
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Register(params) {
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [inActive, setInActive] = useState(false)
    const {user,setUser} =useContext(UserContext);   
    useEffect(() => {
        // email && console.log(email)
        // password1 && console.log(password1)
        // password2 && console.log(password2)
        setInActive(
            email &&
            password1 &&
            password2 &&
            password1 === password2)
        // console.log(isActive)
    }, [email, password1, password2])

    function registerUser(event) {
        event.preventDefault();
        localStorage.setItem('email',email)
        setUser(localStorage.getItem('email'));
        // alert(`Congratulations ${email} you are now registered in our website`);
    }
    return (
        (user)
        ?<Navigate to ="/"/>
        :
       <Container>
            <Row>
                <Col className="col-md-4 col-8 offset-2 offset-md-4 bg-info">
                    <Form onSubmit={registerUser}>

                        <Form.Group className="" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={event => setEmail(event.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="" controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password1}
                                onChange={event => setPassword1(event.target.value)}
                                required />
                        </Form.Group>

                        <Form.Group className="" controlId="password2">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password2}
                                onChange={event => setPassword2(event.target.value)}
                                required />
                        </Form.Group>

                        <Button variant="primary" type="submit" disabled={!inActive}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
     </Container>
    )
};
