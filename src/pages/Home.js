import Banner from "../components/Banner"
import HighLights from "../components/HighLights"
import { Container } from "react-bootstrap"

export default function Home(){
    return (
        <>
            <Container>
            <Banner/>
            <HighLights/>
            </Container>
        </>
    )
}