
import { Container } from "react-bootstrap";
import { useLocation} from "react-router-dom";

export default function Error404() {
     const location = useLocation();
  console.log(location.pathname);
    
    return (
        
    <Container className=''>
    <div className="d-flex flex-column justify-content-center align-items-center mt-5 pt-5 min-vh-90">
      <h1 className=" display">ERROR 404</h1>
      <p>Page "{location.pathname}" Not Found</p>
    </div>
        </Container>

    )
};
