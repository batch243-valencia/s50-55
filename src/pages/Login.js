import { Button, Col, Container, Form, Row } from "react-bootstrap";
import {useEffect, useState,useContext} from 'react'
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
export default function Login() {
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [inActive, setInActive] = useState(false);
    // const[user,setUser] = useState(null)
    const {user,setUser} =useContext(UserContext);
    useEffect(()=>{
        // email && console.log(email)
        // password && console.log(password)
        setInActive(
            email && 
            password)
    },[email,password])

    function authenticate(event){
        event.preventDefault();
        alert(`Welcome ${email} you are now Logged in`);
        localStorage.setItem('email',email)
        setUser(localStorage.getItem("email"))
        setEmail('');
        setPassword('');
    }
    return (
        (user)
        ?<Navigate to ="/"/>
        :<Container>
        <Row>
            <Col className="col-md-4 col-8 offset-2 offset-md-4 bg-info">
                <Form onSubmit={authenticate}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={event=>setEmail(event.target.value)}
                            required
                            />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={event=>setPassword(event.target.value)}
                            required/>
                    </Form.Group>

                    <Button variant="primary" type="submit" disabled={!inActive}>
                        Submit
                    </Button>
                </Form>
            </Col>
        </Row>
        </Container>
    )
};
