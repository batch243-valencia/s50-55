import { Fragment } from 'react'
import CourseCard from '../components/CourseCard'
import coursesData from '../components/data/Courses'
export default function Courses(){  
    // console.log(coursesData)


    const courses = coursesData.map(course=>{
        return (
            <CourseCard key = {course.id} Courseprop={course}/>
        )
    })
    return(
            <Fragment>
                {courses}
            </Fragment>
            
    )
}