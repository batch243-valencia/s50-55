import { Navigate } from "react-router-dom"
import UserContext from "../UserContext";
import {useContext, useEffect} from 'react'

export default function Logout() {
    // localStorage.clear()
    
    const {unSetUser} =useContext(UserContext);
    unSetUser()

    return(
        <Navigate to ='/'/>
    )
};
